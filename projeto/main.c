#include <stdio.h>
#include "estudante.h"
#include "perfil.h"
#include "menus.h"

#define MAX_ESTUDANTES 10 // número máximo de estudantes

int main()
{
    int n_estudantes = 0; // número de estudantes atual (inicialmente é 0)
    Student estudantes[MAX_ESTUDANTES];
    int opcao, i, x;
    while (1)
    {
        opcao = menu1();

        switch (opcao)
        {
        case 1:
            // verificamos primeiro se ainda é possível criar um estudante
            if (n_estudantes == MAX_ESTUDANTES)
            {
                printf("Não é possível criar mais perfis.\n");
            }
            else
            {
                // se temos n_estudantes criados, a próxima posição
                // livre do vetor é n_estudantes
                estudantes[n_estudantes] = criaStudent();
                n_estudantes++; //incrementamos o n_estudantes por 1
            }
            break;
        case 2:
            imprimeStudents(estudantes, n_estudantes);
            break;
        case 3:
            // verificamos se existe algum estudante para apagar
            if (n_estudantes == 0)
            {
                printf("Não existem estudantes para apagar.\n");
            }
            else
            {
                do
                {
                    printf("Insira a posição do estudante a apagar: ");
                    scanf("%d", &x);
                    getchar(); //limpar buffer
                }
                // a posição a apagar tem de estar entre 0 e o número de estudantes atual
                while (x < 0 || x >= n_estudantes);

                // começando na posição i = x, vamos copiar a
                // posição a seguir (i + 1) para a posição atual (i)
                for (i = x; i < n_estudantes - 1; i++)
                {
                    estudantes[i] = estudantes[i + 1];
                }

                n_estudantes--; // reduzimos o número de estudantes atual por 1
            }
            break;
        case 4:
            return 0;
        }
    }
    return 0;
}
