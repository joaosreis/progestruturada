#define TAM 20

typedef struct s_student
{
    char name[TAM];
    char surname[TAM];
    int age;
    int number;
} Student;

void imprimeStudent(Student s);

int nomesStudentIguais(Student s1, Student s2);

Student criaStudent();

void imprimeStudents(Student estudantes[], int n);
