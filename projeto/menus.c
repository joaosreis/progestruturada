#include "menus.h"
#include <stdio.h>

int menu1()
{
    int opcao;
    do
    {
        printf("1 - Criar estudante\n");
        printf("2 - Listar estudantes\n");
        printf("3 - Apagar estudante\n");
        printf("4 - Sair\n");
        printf("Introduza o número da opção: ");
        scanf("%d", &opcao);
        getchar(); // limpar buffer
    } while (opcao < 1 || opcao > 4);

    return opcao;
}
