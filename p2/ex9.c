#include <stdio.h>

int main()
{
    int x, unidades, dezenas;

    printf("Digite um inteiro com dois dígitos: ");
    scanf("%d", &x);

    // Exemplo: 45 / 10 = 4
    dezenas = x / 10;
    // Exemplo: 45 % 10 = 5
    unidades = x % 10;

    printf("%d%d\n", unidades, dezenas);

    return 0;
}
