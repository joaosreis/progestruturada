#include <stdio.h>

int main()
{
    int x, resultado;

    printf("Introduza um valor inteiro: ");
    scanf("%d", &x);

    resultado = x / 3;
    printf("%d", resultado);

    return 0;
}
