#include <stdio.h>

int main()
{
    int valor1, valor2, resultado;

    printf("Digite o valor1: ");
    scanf("%d", &valor1);

    printf("Digite o valor2: ");
    scanf("%d", &valor2);

    printf("%d = %d * %d + %d", valor1, valor2, valor1 / valor2, valor1 % valor2);

    return 0;
}
