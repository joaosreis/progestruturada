#include <stdio.h>

int main()
{
    float x, cm, kg, m, km, celsius;

    printf("Digite um valor real: ");
    scanf("%f", &x);

    cm = x * 2.54;
    kg = x * 0.4539237;
    m = x * 0.3048;
    km = x * 1.609344;
    celsius = (x - 32) / 1.8;

    printf("%f polegadas são %f centímetros.\n", x, cm);
    printf("%f libras são %f quilogramas.\n", x, kg);
    printf("%f pés são %f metros.\n", x, m);
    printf("%f milhas são %f quilómetros.\n", x, km);
    printf("%f graus Fahrenheit são %f graus Celsius.\n", x, celsius);

    return 0;
}
