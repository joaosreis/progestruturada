#include <stdio.h>

int main()
{
    float x;

    printf("Digite um valor real: ");
    scanf("%f", &x);

    printf("%.2f\n", x);

    return 0;
}
