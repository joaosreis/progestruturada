#include <stdio.h>

int main()
{
    float r, area;

    printf("Digite o raio da circunferência: ");
    scanf("%f", &r);

    area = 3.1415 * r * r;

    printf("%f\n", area);

    return 0;
}
