#include <stdio.h>

int main()
{
    int x, resultado;

    printf("Introduza um valor inteiro: ");
    scanf("%d", &x);

    // x % 3 significa "o resto da divisão inteira de x por 3"
    // x / y = quociente
    // x % y = resto
    // x = quociente * y + resto
    resultado = x % 3;
    printf("%d", resultado);

    return 0;
}
