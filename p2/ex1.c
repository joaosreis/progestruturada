#include <stdio.h>

int main()
{
    int x, y, resultado;

    printf("Introduza dois valores inteiros: ");
    scanf("%d %d", &x, &y);

    resultado = x * y;
    printf("%d", resultado);

    return 0;
}
