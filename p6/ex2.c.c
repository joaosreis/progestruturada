#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TAM 20

int main()
{
    char nome[TAM], sobrenome[TAM], nomecompleto_s[2 * TAM + 1], nomecompleto[2 * TAM], output[60], num_s[10];
    int num;
    fputs("Introduza o número de aluno: ", stdout);
    fgets(num_s, 9, stdin);
    num = atoi(num_s);
    fputs("Introduza o primeiro e último nome do aluno: ", stdout);
    fgets(nomecompleto_s, 2 * TAM, stdin);
    strncpy(nomecompleto, nomecompleto_s, strlen(nomecompleto_s) - 1);
    sprintf(output, "Número %d -- %s\n", num, nomecompleto);
    fputs(output, stdout);
    return 0;
}
