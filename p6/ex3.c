#include <stdio.h>
#include <string.h>

#define TAM 20

int main()
{
    char nome[TAM], sobrenome[TAM], nomecompleto[2 * TAM];
    int num;
    printf("Introduza o número de aluno: ");
    scanf("%d", &num);
    printf("Introduza o primeiro e último nome do aluno: ");
    scanf("%s %s", nome, sobrenome);
    strcpy(nomecompleto, nome);
    strcat(nomecompleto, " ");
    strcat(nomecompleto, sobrenome);
    printf("Número %d - %s\n", num, nomecompleto);
    return 0;
}
