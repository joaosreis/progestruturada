#include <stdio.h>
#include <string.h>

void stringCopy(char *destination, const char *origin)
{
    int i = 0;

    do
    {
        destination[i] = origin[i];
    } while (destination[i++] != '\0');
}

int main()
{
    char *s1 = "teste", s2[6];

    stringCopy(s2, s1);

    printf("%s\n", s2);

    return 0;
}
