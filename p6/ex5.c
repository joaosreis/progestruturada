#include <stdio.h>
#include <string.h>

#define TAM 20

int main()
{
    char s[TAM];
    int i, tam;

    printf("Introduza uma string: ");
    scanf("%s", s);

    tam = strlen(s);

    for (i = tam - 1; i >= 0; i--)
    {
        printf("%c", s[i]);
    }

    printf("\n");

    return 0;
}
