#include <stdio.h>

int stringLength(const char *str)
{
    int i = 0;

    while (str[i] != '\0') // enquanto não estivermos no fim da string
    {
        i++;
    }

    return i;
}

int main()
{
    char *teste = "TESTE123";

    printf("%s tem tamanho %d\n", teste, stringLength(teste));

    return 0;
}
