#include <stdio.h>
#include <string.h>

#define TAM 20

int main()
{
    char s[TAM], c;
    int i, tam, n = 0;

    printf("Introduza uma string: ");
    fgets(s, TAM, stdin);
    printf("Introduza um carater: ");
    scanf("%c", &c);

    tam = strlen(s);

    for (i = 0; i < tam; i++)
    {
        if (s[i] == c)
        {
            n++;
        }
    }

    printf("Número de ocorrências do carater %c na string %s: %d\n", c, s, n);

    return 0;
}
