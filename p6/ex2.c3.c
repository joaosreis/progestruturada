#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define TAM 20

void limparLinha(char s[])
{
    s[strlen(s) - 1] = '\0';
}

int main()
{
    char nome[TAM], sobrenome[TAM], nomecompleto[2 * TAM], output[60], num_s[10];
    int num;
    fputs("Introduza o número de aluno: ", stdout);
    scanf("%d", &num);
    fgetc(stdin); // limpar o \n do buffer do stdin
    fputs("Introduza o primeiro e último nome do aluno: ", stdout);
    fgets(nomecompleto, 2 * TAM - 1, stdin);
    limparLinha(nomecompleto);
    sprintf(output, "Número %d -- %s\n", num, nomecompleto);
    fputs(output, stdout);
    return 0;
}
