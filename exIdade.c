#include <stdio.h>
#include <time.h>

int main()
{
    int dia_nascimento, mes_nascimento, ano_nascimento, dia_atual, mes_atual, ano_atual, idade;
    time_t tempo_atual_s;
    struct tm *tempo_atual;

    printf("Introduza a data de nascimento (dd mm aaaa): ");
    scanf("%d %d %d", &dia_nascimento, &mes_nascimento, &ano_nascimento);

    tempo_atual_s = time(NULL);              // numero de segundos desde 1 jan 1970
    tempo_atual = localtime(&tempo_atual_s); // estrutura de dados com o momento atual

    /* tempo_atual é um apontador para uma estrutura, e não a
       estrutura em si. Como tal, para acedermos a um elemento
       da estrutura usamos -> em vez de . */
    dia_atual = tempo_atual->tm_mday;        // tm_mday -> dia do mês
    mes_atual = tempo_atual->tm_mon + 1;     // tm_mon -> mês do ano (está entre 0 e 11)
    ano_atual = tempo_atual->tm_year + 1900; // tm_year -> anos desde 1900

    printf("Hoje estamos a %d/%d/%d\n", dia_atual, mes_atual, ano_atual);

    // Calculamos a idade com base apenas no ano.
    idade = ano_atual - ano_nascimento;

    // Agora vemos se a pessoa já fez anos este ano.
    // Se não fez, subtraímos 1 à idade.

    // se estamos antes do mês de nascimento
    if (mes_atual < mes_nascimento)
    {
        idade--;
    }
    // se estamos no mês de nascimento mas antes do dia de nascimento
    else if (mes_atual == mes_nascimento && dia_atual < dia_nascimento)
    {
        idade--;
    }

    printf("Idade: %d\n", idade);

    return 0;
}
