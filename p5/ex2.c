#include <stdio.h> 
#define DIM2 3

/* função para ler matriz exercecio 2 */
void lerVectorBidimensional(int vector[][DIM2], int dim1){
  int i,j;
  for(i=0;i<dim1;i++){
    for(j=0;j<DIM2;j++){
      printf("Introduza o valor para o índice (%d,%d) da matriz: ",i,j);
      scanf("%d",&vector[i][j]);
    }
  }
}
