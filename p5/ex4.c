#include <stdio.h> 
#define DIM2 3

void imprimeVetorBidimensional(int mat[][DIM2],int dim1){
  int i,j;
  for(i=0;i<dim1;i++){
    for(j=0;j<DIM2;j++){
      printf("%d ",mat[i][j]);
    }
    printf("\n");
  }
}
