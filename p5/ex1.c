#include <stdio.h> 

/* alternativa com ciclo for */
void lerVectorFor(int vector[], int tamanho){
  int i;
  for(i=0; i< tamanho; i++){
    printf("Introduza o valor para o índice %d do vector: ",i);
    scanf("%d",&vector[i]);
  }
}

/* alternativa com ciclo  while */
void lerVectorWhile(int vector[], int tamanho){
  int i=0;
  while(i<tamanho){
    printf("Introduza o valor para o índice %d do vector: ",i);
    scanf("%d",&vector[i]);
    i++;
  }
}
