#include <stdio.h> 
#include <stdlib.h>
#include <time.h>
#define DIM1 2
#define DIM2 3

void matrizTransposta(int a[DIM1][DIM2], int b[DIM2][DIM1]){
  int i,j;
  for(i=0;i<DIM1;i++){
    for(j=0;j<DIM2; j++){
      b[j][i] = a[i][j];
    }
  }
}
