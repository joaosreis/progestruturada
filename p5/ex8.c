#include <stdio.h> 
#define DIM2 3

void minMax(int mat[][DIM2], int dim1, int* min, int* max){
  int i,j;
  *min = mat[0][0];
  *max = mat[0][0];
  for(i=0;i<dim1;i++){
    for(j=0;j<DIM2;j++){
      if(mat[i][j] > *max){
        *max = mat[i][j];
      }
      if(mat[i][j] < *min){
        *min = mat[i][j];
      }
    }
  }
}
