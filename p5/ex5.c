#include <stdio.h> 
/* exercicio 5 */
/*
  variavel auxiliar para contar o numero de zeros
  percorrer o vector
  para cada posição --> comparar o valor com zero, em caso verdadeiro incrementar a variável auxiliar
  retornar a variável auxiliar
*/

int nZeros(int v[], int size){
  int i,n=0;
  for(i=0;i<size;i++){
    if(v[i] == 0){
      n++;
    }
  }
  return n;
}
