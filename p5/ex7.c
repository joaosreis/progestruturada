#include <stdio.h> 
#define DIM2 3

float meanMatrix(int mat[][DIM2], int dim1){
  float media=0;
  int i,j;
  for(i=0;i<dim1;i++){
    for(j=0;j<DIM2;j++){
      media = media + mat[i][j];
    }
  }
  media = media / (dim1*DIM2);
  return media;
}
