/* Algoritmo
    1. Escrever no ecrã a string: "Introduz a primeira nota: "
    2. Ler um valor inteiro e guardar na variável nota_1
    3. Escrever no ecrã a string: "Introduz a segunda nota: "
    4. Ler um valor inteiro e guardar na variável nota_2
    5. Guardar na variável float resultado o valor de (nota_1 + nota_2) / 2
    6. Escrever no ecrã a string: "Média das notas: resultado"
*/

#include <stdio.h>

int main()
{
    int nota_1, nota_2;
    float resultado;

    printf("Introduz a primeira nota: ");
    scanf("%d", &nota_1);

    printf("Introduz a segunda nota: ");
    scanf("%d", &nota_2);

    // Divimos por 2.0 (float) pois como nota_1 e nota_2 são int,
    // se dividirmos por 2 (inteiro) ele faz divisão inteira por
    // ser tudo inteiro
    resultado = (nota_1 + nota_2) / 2.0;

    printf("Média das notas: %f\n", resultado);

    return 0;
}
