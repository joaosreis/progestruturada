/* Algoritmo
    1. Escrever no ecrã a string: "Introduz a altura do triângulo: "
    2. Ler um valor float e guardar na variável alt_triangulo
    3. Escrever no ecrã a string: "Introduz o comprimento da base do triângulo: "
    4. Ler um valor float e guardar na variável base_triangulo
    5. Guardar na variável float o valor de alt_triangulo * base_triangulo / 2
    6. Escrever no ecrã a string: "A área do triângulo é area_triangulo."
*/

#include <stdio.h>

int main()
{
    float alt_triangulo, base_triangulo, area_triangulo;

    printf("Introduz a altura do triângulo: ");
    scanf("%f", &alt_triangulo);

    printf("Introduz o comprimento da base do triângulo: ");
    scanf("%f", &base_triangulo);

    area_triangulo = alt_triangulo * base_triangulo / 2;

    printf("A área do triângulo é %f.", area_triangulo);

    return 0;
}
