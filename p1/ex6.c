#include <stdio.h>

int main()
{
    int num_aluno;

    printf("Introduz o teu número de aluno: ");

    scanf("%d", &num_aluno);

    printf("Bem-vindo à aula de programação aluno %d", num_aluno);

    return 0;
}
