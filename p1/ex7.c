/* Algoritmo
    1. Escrever no ecrã a string: "Introduz o teu número de aluno: "
    2. Ler um valor inteiro e guardar na variável num_aluno
    3. Escrever no ecrã a string: "Introduz a tua idade: "
    4. Ler um valor inteiro e guardar na variável anos_aluno
    5. Escrever no ecrã a string: "Introduz a tua altura: "
    6. Ler um valor real e guardar na variável alt_aluno
    7. Escrever no ecrã a string: "Olá! Tu és o aluno num_aluno, tens anos_aluno anos e medes alt_aluno."
*/

#include <stdio.h>

int main()
{
    int num_aluno, anos_aluno;
    float alt_aluno;

    printf("Introduz o teu número de aluno: ");
    scanf("%d", &num_aluno);

    printf("Introduz a tua idade: ");
    scanf("%d", &anos_aluno);

    printf("Introduz a tua altura: ");
    scanf("%f", &alt_aluno);

    printf("Olá! Tu és o aluno %d, tens %d anos e medes %f.\n", num_aluno, anos_aluno, alt_aluno);

    return 0;
}
