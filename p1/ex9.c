/* Algoritmo
    1. Escrever no ecrã a string: "Boa Tarde!"
    2. Escrever no ecrã a string: "Introduz um número inteiro: "
    3. Ler um valor inteiro e guardar na variável x
    4. Escrever no ecrã a string: "Introduz um número real: "
    5. Ler um valor float e guardar na variável y
    6. Guardar na variável float resultado o valor de x * y
    7. Escrever no ecrã a string: "x * y = resultado"
*/

#include <stdio.h>

int main()
{
    int x;
    float y, resultado;

    printf("Boa Tarde!\n");

    printf("Introduz um número inteiro: ");
    scanf("%d", &x);

    printf("Introduz um número real: ");
    scanf("%f", &y);

    resultado = x * y;

    printf("%d * %f = %f\n", x, y, resultado);

    return 0;
}
