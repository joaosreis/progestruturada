#include <stdio.h>

int main()
{
    int ano;

    // Face ao algoritmo original, falta inicializar a variável com
    // algum valor
    ano = 2020;

    printf("%d\n", ano);

    return 0;
}
