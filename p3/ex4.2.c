#include <stdio.h>

int main()
{
    float x, y;
    int opcao;

    printf("Introduza dois valores reais: ");
    scanf("%f %f", &x, &y);

    do
    {
        printf("Escolha a operação\n");
        printf("1 - Soma\n");
        printf("2 - Subtração\n");
        printf("3 - Multiplicação\n");
        printf("4 - Divisão\n");
        printf("Introduza uma opção: ");
        scanf("%d", &opcao);
    }
    // enquanto o utilizador não introduzir uma opção válida
    while (opcao < 1 || opcao > 4);

    switch (opcao)
    {
    case 1:
        printf("%f + %f = %f\n", x, y, x + y);
        break;
    case 2:
        printf("%f - %f = %f\n", x, y, x - y);
        break;
    case 3:
        printf("%f * %f = %f\n", x, y, x * y);
        break;
    case 4:
        // Verificar se y é 0, quando se trata de floats
        if (y < 0.0000001 && y > -0.0000001)
        {
            printf("Erro: divisão por zero\n");
        }
        else
        {
            printf("%f / %f = %f\n", x, y, x / y);
        }
    }

    return 0;
}
