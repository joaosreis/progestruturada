#include <stdio.h>

int main()
{
    int x, i;

    printf("Introduza um valor inteiro: ");
    scanf("%d %d", &x);

    // i++ -> i = i + 1
    for (i = 0; i < x; i++)
    {
        if (i % 2 == 0)
        {
            printf("%d\n", i);
        }
    }

    return 0;
}
