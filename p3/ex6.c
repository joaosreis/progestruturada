#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    float x, y, resultado;
    int opcao;

    /* Inicializar o gerador com uma seed.
       Ao usarmos o time, garantimos que é diferente de cada vez
       que se chama o programa */
    srand(time(NULL));

    printf("Introduza o primeiro valor: ");
    scanf("%f", &x);

    printf("Introduza o segundo valor: ");
    scanf("%f", &y);

    opcao = rand() % 4 + 1; // 1 <= opcao <= 4

    switch (opcao)
    {
    case 1:
        resultado = x + y;
        break;
    case 2:
        resultado = x - y;
        break;
    case 3:
        resultado = x * y;
        break;
    case 4:
        // Verificar se y == 0, tendo em conta que é um float
        if (y < 0.000000001 && y > -0.000000001)
        {
            printf("Erro: divisão por zero\n");
            return 1;
        }
        resultado = x / y;
    }

    printf("Resultado: %f\n", resultado);

    return 0;
}
