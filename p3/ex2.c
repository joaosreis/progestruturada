#include <stdio.h>

int main()
{
    int x;

    printf("Introduza um valor inteiro: ");
    scanf("%d %d", &x);

    // Um número é par se for divisível por 2 (o resta da divisão inteira por 2 é 0)
    if (x % 2 == 0)
    {
        printf("Par\n");
    }
    else
    {
        printf("Ímpar\n");
    }

    return 0;
}
