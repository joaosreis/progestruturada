#include <stdio.h>

int main()
{
    int ano;

    do
    {
        printf("Introduza um ano: ");
        scanf("%d", &ano);
    }
    // enquanto o utilizador não introduzir um ano válido
    while (ano < 1000 || ano > 2999);

    // se o ano não for divisível por 4
    if (ano % 4 != 0)
    {
        printf("365 dias\n");
    }
    // se o ano não acaba em 00 (mesmo que dizer que não é divisível por 100)
    else if (ano % 100 != 0)
    {
        printf("366 dias\n");
    }
    // se o ano é divisível por 400
    else if (ano % 400 == 0)
    {
        printf("366 dias\n");
    }
    else
    {
        printf("365 dias\n");
    }

    return 0;
}
