#include <stdio.h>

int main()
{
    int x, y;
    float resultado;

    printf("Introduza dois valores inteiros: ");
    scanf("%d %d", &x, &y);

    if (y == 0)
    {
        printf("Erro: divisão por zero\n");
    }
    else
    {
        /* Como x e y são inteiros, temos de fazer cast a um deles
           para float para a divisão ser real e não inteira.
        */
        resultado = (float)x / y;
        printf("Resultado: %f\n", resultado);
    }

    return 0;
}
