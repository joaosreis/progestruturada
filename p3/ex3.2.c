#include <stdio.h>

int main()
{
    int x, i;

    printf("Introduza um valor inteiro: ");
    scanf("%d %d", &x);

    i = 0;

    while (i < x)
    {
        if (i % 2 == 0)
        {
            printf("%d\n", i);
            i++;
        }
    }

    return 0;
}
