/* Alínea 2.d:
Declare um vetor do tipo Student com tamanho 10 
Leia um inteiro n do teclado que deverá estar entre 1 e 10
Preencha o vetor com n estruturas Student com recurso à função criarStudent

Alínea 2.e:
Crie uma função que receba como parâmetro um vetor do tipo Student e imprima
no ecrã o conteúdo de cada estrutura Student contida no vetor. Teste a função
com o programa criado na questão anterior. */

#include <stdio.h>
#include <string.h>

#define TAM 20

typedef struct s_student
{
    char name[TAM];
    char surname[TAM];
    int age;
    int number;
} Student;

void imprimeStudent(Student s)
{
    printf("Nome: %s %s\nIdade: %d\nNúmero: %d\n", s.name, s.surname, s.age, s.number);
}

// devolve 0 se forem diferentes, 1 se forem iguais
int nomesStudentIguais(Student s1, Student s2)
{
    // se s1.name for igual a s2.name
    if (strcmp(s1.name, s2.name) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* 1. Criar uma função que cria um estudante a partir dos
      dados inseridos pelo utilizador.
   2. Na função main, declarar o array de estudantes (com
      tamanho à vossa escolha) e implementar o código que
      vá preenchendo as várias posições usando a função
      criada em 1.
*/

Student criaStudent()
{
    Student s;

    printf("Introduza o primeiro nome: ");
    fgets(s.name, TAM - 1, stdin);
    s.name[strlen(s.name) - 1] = '\0'; // substituir o \n no fim por \0
    printf("Introduza o segundo nome: ");
    fgets(s.surname, TAM - 1, stdin);
    s.surname[strlen(s.surname) - 1] = '\0';
    printf("Introduza a idade: ");
    scanf("%d", &s.age);
    printf("Introduza o número: ");
    scanf("%d", &s.number);
    fgetc(stdin); // getchar();

    return s;
}

void imprimeStudents(Student estudantes[], int n)
{
    int i;

    for (i = 0; i < n; i++)
    {
        printf("Estudante %d\n", i);
        imprimeStudent(estudantes[i]);
    }
}

int main()
{
    Student estudantes[10];
    int i, n;

    do
    {
        printf("Introduza um valor entre 1 e 10: ");
        scanf("%d", &n);
        getchar();
    } while (n < 1 || n > 10);

    for (i = 0; i < n; i++)
    {
        printf("Criar estudante %d\n", i);
        estudantes[i] = criaStudent();
    }

    imprimeStudents(estudantes, n);

    return 0;
}
