#include <stdio.h>

typedef struct s_complex
{
    float real;
    float imaginary;
} Complex;

void imprimeComplexo(Complex n)
{
    printf("%f + %f * i\n", n.real, n.imaginary);
}

Complex somaComplexos(Complex n1, Complex n2)
{
    /*  n1 = x1 + y1 * i
        n2 = x2 + y2 * i

        n3 = n1 + n2 = (x1 + x2) + ((y1 + y2) * i)
    */
    Complex n3;

    n3.real = n1.real + n2.real;
    n3.imaginary = n1.imaginary + n2.imaginary;

    return n3;
}

int main()
{
    Complex n1, n2;

    // n1 = 2.5 + 3.6 * i
    n1.real = 2.5;
    n1.imaginary = 3.6;

    // n2 = 3.7 - 1.6 * i
    n2.real = 3.7;
    n2.imaginary = -1.6;

    imprimeComplexo(somaComplexos(n1, n2));

    return 0;
}
