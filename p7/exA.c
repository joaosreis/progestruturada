/* Exercício A: Escrever um programa que pergunte ao utilizador
   se quer inserir um estudante novo, listar todos os estudante,
   remover um estudante, ou sair (com base no código do exercício
   2 da prática 7). */

#include <stdio.h>
#include <string.h>

#define TAM 20

#define TAM_ESTUDANTES 10

typedef struct s_student
{
    char name[TAM];
    char surname[TAM];
    int age;
    int number;
} Student;

void imprimeStudent(Student s)
{
    printf("Nome: %s %s\nIdade: %d\nNúmero: %d\n", s.name, s.surname, s.age, s.number);
}

// devolve 0 se forem diferentes, 1 se forem iguais
int nomesStudentIguais(Student s1, Student s2)
{
    // se s1.name for igual a s2.name
    if (strcmp(s1.name, s2.name) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* 1. Criar uma função que cria um estudante a partir dos
      dados inseridos pelo utilizador.
   2. Na função main, declarar o array de estudantes (com
      tamanho à vossa escolha) e implementar o código que
      vá preenchendo as várias posições usando a função
      criada em 1.
*/

Student criaStudent()
{
    Student s;

    printf("Introduza o primeiro nome: ");
    fgets(s.name, TAM - 1, stdin);
    s.name[strlen(s.name) - 1] = '\0'; // substituir o \n no fim por \0
    printf("Introduza o segundo nome: ");
    fgets(s.surname, TAM - 1, stdin);
    s.surname[strlen(s.surname) - 1] = '\0';
    printf("Introduza a idade: ");
    scanf("%d", &s.age);
    printf("Introduza o número: ");
    scanf("%d", &s.number);
    fgetc(stdin); // getchar();

    return s;
}

void imprimeStudents(Student estudantes[], int n)
{
    int i;

    for (i = 0; i < n; i++)
    {
        printf("Estudante %d\n", i);
        imprimeStudent(estudantes[i]);
    }
}

int main()
{
    Student estudantes[TAM_ESTUDANTES];
    int n, opcao, x, i;

    // numero de estudantes existentes no inicio do programa
    n = 0;

    while (1)
    {
        printf("1 - Inserir estudante\n");
        printf("2 - Listar estudantes\n");
        printf("3 - Remover um estudante\n");
        printf("4 - Sair\n");
        printf("Introduza uma opção: ");
        scanf("%d", &opcao);
        getchar();

        switch (opcao)
        {
        case 1:
            // inserir estudante

            // verifico se ainda é possível criar mais estudantes
            if (n == TAM_ESTUDANTES)
            {
                printf("Não é possível criar mais estudantes\n");
            }
            else
            {
                estudantes[n] = criaStudent();
                n++;
            }
            break;
        case 2:
            // listar estudantes

            imprimeStudents(estudantes, n);
            break;
        case 3:
            // remover um estudante

            if (n == 0)
            {
                printf("Não existem estudantes para remover\n");
            }
            else
            {
                do
                {
                    printf("Indique o estudante que quer remover: ");
                    scanf("%d", &x);
                    getchar();
                } while (x < 0 || x >= n); // enquanto o x é inválido

                for (i = x; i < n - 1; i++)
                {
                    estudantes[i] = estudantes[i + 1];
                }

                n--;
            }
            break;
        default:
            return 0;
        }
    }

    return 0;
}
