#include <stdio.h>
#include <string.h>

#define TAM 20

typedef struct s_student
{
    char name[TAM];
    char surname[TAM];
    int age;
    int number;
} Student;

void imprimeStudent(Student s)
{
    printf("Nome: %s %s\nIdade: %d\nNúmero: %d\n", s.name, s.surname, s.age, s.number);
}

// devolve 0 se forem diferentes, 1 se forem iguais
int nomesStudentIguais(Student s1, Student s2)
{
    // se s1.name for igual a s2.name
    if (strcmp(s1.name, s2.name) == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* 1. Criar uma função que cria um estudante a partir dos
      dados inseridos pelo utilizador.
   2. Na função main, declarar o array de estudantes (com
      tamanho à vossa escolha) e implementar o código que
      vá preenchendo as várias posições usando a função
      criada em 1.
*/

Student criaStudent()
{
    Student s;

    printf("Introduza o primeiro nome: ");
    fgets(s.name, TAM - 1, stdin);
    s.name[strlen(s.name) - 1] = '\0'; // substituir o \n no fim por \0
    printf("Introduza o segundo nome: ");
    fgets(s.surname, TAM - 1, stdin);
    s.surname[strlen(s.surname) - 1] = '\0';
    printf("Introduza a idade: ");
    scanf("%d", &s.age);
    printf("Introduza o número: ");
    scanf("%d", &s.number);
    fgetc(stdin); // getchar();

    return s;
}

int main()
{
    Student estudantes[10];
    int i;

    for (i = 0; i < 5; i++)
    {
        printf("Criar estudante %d\n", i);
        estudantes[i] = criaStudent();
    }

    return 0;
}
