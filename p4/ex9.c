#include <stdio.h>

float adicao(float a, float b){
  return a + b;
}

float subtracao(float a, float b){
  return a - b;
}

float multiplicacao(float a, float b){
  return a * b;
}

float divisao(float a, float b){
  if (b == 0){
    printf("ERRO! não se pode dividir por 0!!\n");
  }else return a / b;
}

/* função do exerício 4 */
int pedeValor(int n){
  int x;

  while (1){
    printf("Introduza um valor: ");
    scanf("%d", &x);
    // Verificar se x está entre 0 e n
    if (x >= 0 && x <= n){
        // Devolve x e termina a execução da função
        return x;
    } else{
        printf("Valor inválido\n");
    }
  }

}

void menu(){
  int opcao;
  float a,b,result;

  while (1) {
    printf("1 -> Adição\n");
    printf("2 -> Subtracção\n");
    printf("3 -> Multiplição\n");
    printf("4 -> Divião\n\n");
    printf("0 -> Sair\n");
    opcao = pedeValor(4);
    if (opcao == 0){
      return; // podemos logo parar a execução da função pois o utilizador deseja sair
    }
    printf("Introduza um valor real: ");
    scanf("%f",&a);
    printf("Introduza outro valor real: ");
    scanf("%f",&b);
    switch (opcao) {
    case 1:
      result = adicao(a,b);
      printf("%.2f + %.2f = %.2f\n",a,b,result);
      break;
    case 2:
      result = subtracao(a,b);
      printf("%.2f - %.2f = %.2f\n",a,b,result);
      break;
    case 3:
      result = multiplicacao(a,b);
      printf("%.2f * %.2f = %.2f\n",a,b,result);
      break; 
    case 4:
      result = divisao(a,b);
      printf("%.2f + %.2f = %.2f\n",a,b,result);
      break;   
    default:
      break;
    }
  } 

}

int main(){
  menu();
  return 0;
}
