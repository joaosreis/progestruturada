#include <stdio.h>

int eCarater(char c)
{
    // 'A' <= c <= 'Z'
    if (c >= 'A' && c <= 'Z')
    {
        return 1;
    }
    else if (c >= 'a' && c <= 'z')
    {
        return -1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    char i;
    int r;

    printf("Introduza o carater: ");
    scanf("%c", &i);

    r = eCarater(i);

    if (r == 1)
    {
        printf("Carater maiusculo\n");
    }
    else if (r == -1)
    {
        printf("Carater minusculo\n");
    }
    else if (r == 0)
    {
        printf("Não é uma letra\n");
    }

    return 0;
}
