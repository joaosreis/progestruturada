#include <stdio.h>

int ePositivo(int a);

int main()
{
    int x, resultado;

    printf("Introduza um valor inteiro: ");
    scanf("%d", &x);

    resultado = ePositivo(x);

    if (resultado == 1)
    {
        printf("Maior ou igual a zero\n");
    }
    else if (resultado == 0)
    {
        printf("Menor que zero\n");
    }

    return 0;
}

int ePositivo(int a)
{
    int r;

    if (a >= 0)
    {
        r = 1;
    }
    else if (a < 0)
    {
        r = 0;
    }

    return r;
}
