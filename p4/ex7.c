#include <stdio.h>

int eCarater(char c){
    // 'A' <= c <= 'Z'
    if (c >= 'A' && c <= 'Z'){
        return 1;
    } // 'a' <= c <= 'c'
    else if (c >= 'a' && c <= 'z')    {
        return -1;
    }else {
        return 0;
    }
}

char inverseCapitalization(){
  char c,flag;
  do{
    printf("Introduza o caracter desejado:\n");
    scanf("%c",&c);
    flag = eCarater(c);
  }while(flag == 0); // pedir caracter ao utilizador enquanto este não for válido

  if(flag == 1){
    return c+32; // de maiuscula para minuscula
  } else {
    return c-32; // de minuscula para maiucula
  }
  
}

int main(){
  char c = inverseCapitalization();
  printf("O caracter invertido é: %c\n",c);
  return 0;
}
