/*
    Entrada: dia1, mes1, ano1, dia2, mes2, ano2
    Saída: 0 se alguma das datas for inválida, 1 se a primeira for mais recente,
           2 se a segunda for mais recente, 3 caso contrário (são iguais)

    Algoritmo:
    1. Verificar se as datas são válidas. Se alguma não for, devolver 0.
    2. Comparar o ano das duas datas. Se ano1 > ano2, devolver 1. Se ano2 > ano1, devolver 2.
       Caso contrário, continuamos.
    3. Comparar o mês das duas datas. Se mes1 > mes2, devolver 1. Se mes2 > mes1, devolver 2.
       Caso contrário, continuamos.
    4. Comparar o dia das duas datas. Se dia1 > dia2, devolver 1. Se dia2 > dia1, devolver 2.
       Caso contrário, devolver 3 (datas iguais).
*/

#include <stdio.h>

// Devolve 1 se ano for bissexto, devolve 0 caso contrário
int eBissexto(int ano)
{
    // Se o resto da divisão inteira por 4 for diferente de 0, ano não é divisível por 4
    if (ano % 4 != 0)
    {
        return 0;
    }
    else if (ano % 100 != 0)
    {
        return 1;
    }
    else if (ano % 400 == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int dataValida(int dia, int mes, int ano)
{
    // ano tem de ser positivo
    if (ano < 0)
    {
        return 0;
    }

    if (mes >= 1 && mes <= 12)
    {
        // Se for fevereiro
        if (mes == 2)
        {
            // Temos de ver se é bissexto
            if (eBissexto(ano))
            {
                if (dia >= 1 && dia <= 29)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            // Se não for bissexto
            else
            {
                if (dia >= 1 && dia <= 28)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        // Se não for fevereiro
        else
        {
            // Verificar se é mês de 31 dias
            if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
            {
                // Verificar se o dia está entre 1 e 31
                if (dia >= 1 && dia <= 31)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            // Casos em que o mês tem 30 dias
            else
            {
                // Verificar se o dia está entre 1 e 30
                if (dia >= 1 && dia <= 30)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    // Mês não está entre 1 e 12
    else
    {
        return 0;
    }
}

int maisRecente(int dia1, int mes1, int ano1, int dia2, int mes2, int ano2)
{
    // 1.
    // Se alguma delas não for válida
    if (!dataValida(dia1, mes1, ano1) || !dataValida(dia2, mes2, ano2))
    {
        return 0;
    }

    // 2.
    if (ano1 > ano2)
    {
        return 1;
    }
    else if (ano2 > ano1)
    {
        return 2;
    }
    // 3.
    else if (mes1 > mes2)
    {
        return 1;
    }
    else if (mes2 > mes1)
    {
        return 2;
    }
    // 4.
    else if (dia1 > dia2)
    {
        return 1;
    }
    else if (dia2 > dia1)
    {
        return 2;
    }
    else
    {
        return 3;
    }
}

int main()
{
    int r;

    r = maisRecente(26, 2, 1997, 8, 1, 2021);

    printf("%d\n", r);

    return 0;
}
