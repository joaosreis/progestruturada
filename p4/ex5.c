#include <stdio.h>

// Devolve 1 se ano for bissexto, devolve 0 caso contrário
int eBissexto(int ano)
{
    // Se o resto da divisão inteira por 4 for diferente de 0, ano não é divisível por 4
    if (ano % 4 != 0)
    {
        return 0;
    }
    else if (ano % 100 != 0)
    {
        return 1;
    }
    else if (ano % 400 == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int dataValida(int dia, int mes, int ano)
{
    // ano tem de ser positivo
    if (ano < 0)
    {
        return 0;
    }

    if (mes >= 1 && mes <= 12)
    {
        // Se for fevereiro
        if (mes == 2)
        {
            // Temos de ver se é bissexto
            if (eBissexto(ano))
            {
                if (dia >= 1 && dia <= 29)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            // Se não for bissexto
            else
            {
                if (dia >= 1 && dia <= 28)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        // Se não for fevereiro
        else
        {
            // Verificar se é mês de 31 dias
            if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
            {
                // Verificar se o dia está entre 1 e 31
                if (dia >= 1 && dia <= 31)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            // Casos em que o mês tem 30 dias
            else
            {
                // Verificar se o dia está entre 1 e 30
                if (dia >= 1 && dia <= 30)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    // Mês não está entre 1 e 12
    else
    {
        return 0;
    }
}

int main()
{
    int dia, mes, ano;

    printf("Introduzir dia, mês e ano separado por espaço: ");
    scanf("%d %d %d", &dia, &mes, &ano);

    printf("%d\n", dataValida(dia, mes, ano));

    return 0;
}
