#include <stdio.h>

int pedeValor(int n)
{
    int x;

    while (1)
    {
        printf("Introduza um valor: ");
        scanf("%d", &x);

        // Verificar se x está entre 0 e n
        if (x >= 0 && x <= n)
        {
            // Devolve x e termina a execução da função
            return x;
        }
        else
        {
            printf("Valor inválido\n");
        }
    };
}
