#include <stdio.h>

// Variáveis globais
int a, b;

int soma(int a, int b)
{
    int resultado;
    resultado = a + b;
    return resultado;
}

int main()
{
    int resultado, a, b;

    printf("Introduza dois valores inteiros: ");
    scanf("%d %d", &a, &b);

    resultado = soma(5, 7);

    printf("Resultado: %d\n", resultado);

    return 0;
}
