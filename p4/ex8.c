#include <stdio.h>

/* função do exercicio 2 */
int ePositivo(int a){
    int r;

    if (a >= 0) {
        r = 1;
    } else if (a < 0){
        r = 0;
    }
    return r;
}

int fibo(int n){
  if (n == 0){
    return 0;
  } else if(n == 1){
    return 1;
  } else return fibo(n-1) + fibo(n-2);
}

int main(){
  int n,result;

  do {
    printf("Introduza a ordem do elemento que prentende:\n");
    scanf("%d",&n);
  } while (ePositivo(n) == 0);

  result = fibo(n);

  printf("O elemento de ordem %d da sequência de Fibonacci é: %d\n",n,result);

  return 0;
}
